package main

import (
	"fmt"
	"github.com/eiannone/keyboard"
)

func main() {
	err := keyboard.Open()
	if err != nil {
		panic(err)
	}
	defer keyboard.Close()
	for {
		fmt.Print("input  : ")
		var x int
		_, err := fmt.Scanln(&x)
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println("output : ")
		for i := 0; i < x; i++ {
			for j := 0; j <= i; j++ {
				fmt.Print("*")
			}
			fmt.Println()
		}
		fmt.Println("Next ? Click any key except enter and escape")
		_, key, _ := keyboard.GetKey()
		if key == keyboard.KeyEsc {
			fmt.Print("Done")
			break
		} else if key == keyboard.KeyEnter {
			fmt.Print("Done")
			break
		}
	}
}